let c = 2;
let b = 8;
console.log(`The cube of ${c} is ${b}`);

let address = '258 Washington Ave NW, California 90011';

console.log(`I live at ${address}`);

let weight = '1075 kgs';
let measure = '20 ft 3 in.'

console.log(`Lolong was a saltwater crocodile. He weighed at ${weight} with a measurement of ${measure}`);


const numbers = [1,2,3,4,5];

const initialValue = 0;
const sumWithInitial = numbers.reduce(
  (accumulator, currentValue) => accumulator + currentValue,
  initialValue
);


numbers.forEach((number) => {
	console.log(number);
})
console.log(sumWithInitial);

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}


const newDog = new Dog("Frankie",5,"Miniature Dachshund");
console.log(newDog);
